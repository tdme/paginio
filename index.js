/**
 * Created by cecheveria on 06/21/16.
 */
var Paginate = require('./lib/paginate');
var Scroller = require('./lib/scroller');

module.exports.Page = Paginate.Page;
module.exports.ResponseBase = Paginate.ResponseBase;
module.exports.ResponsePage = Paginate.ResponsePage;
module.exports.Buffer = Paginate.Buffer;
module.exports.Scroller = Scroller;
