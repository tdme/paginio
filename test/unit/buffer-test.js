/**
 * Created by cecheveria on 10/21/16.
 */
var Buffer = require('../../lib/paginate').Buffer.create(),
    testData = require('../data/testData')
    ;
var invalidDataSets = Object.keys(testData.invalidDataSets).map(function(key) {
        return testData.invalidDataSets[key];
    });
var paginateByPagenumberTestCases = Object.keys(testData.paginateByPagenumber).map(function(key) {
        return testData.paginateByPagenumber[key];
    });
var items = testData.dataSet;

describe('Buffer Object', () => {

    invalidDataSets
    .forEach(function (paginateSet) {
        describe('Buffer with "' +paginateSet.dataSet+ '" dataSet ' + (paginateSet.testComments || ''), () => {
            var responsePage = Buffer.getPage(paginateSet.dataSet, 4, 1);

            it('must be response.error=' + paginateSet.error, () => {
                expect(responsePage.error).to.equal(paginateSet.error);
            });
            it('must be response.msg="' + paginateSet.msg + '"', () => {
                expect(responsePage.msg).to.equal(paginateSet.msg);
            });
            it('data.pageSize must be ' + paginateSet.pageSize, () => {
                expect(responsePage.data.pageSize).to.equal(paginateSet.pageSize);
            });
            it('data.pageNumber must be ' + paginateSet.pageNumber, () => {
                expect(responsePage.data.pageNumber).to.equal(paginateSet.pageNumber);
            });
            it('data.totalRows must be ' + paginateSet.totalRows, () => {
                expect(responsePage.data.totalRows).to.equal(paginateSet.totalRows);
            });
            it('data.totalPages must be ' + paginateSet.totalPages, () => {
                expect(responsePage.data.totalPages).to.equal(paginateSet.totalPages);
            });
            it((paginateSet.page && paginateSet.page.length) + ' items must be returned in the page', () => {
                expect( (responsePage.data.page && responsePage.data.page.length) ).to.equal(paginateSet.page && paginateSet.page.length);
            });
            it('content must be empty on current page', () => {
                expect(responsePage.data.page).to.deep.equal(paginateSet.page);
            });
        });
    });

    paginateByPagenumberTestCases
    .forEach(function (paginateSet) {

        describe('Buffer with "' + paginateSet.pageSize + '" items per page ' + (paginateSet.testComments || ''), () => {

            paginateSet.pages.forEach(function (page) {
                describe('Getting page #' + page.pageNumber, () => {
                    var responsePage = Buffer.getPage(items, paginateSet.pageSize, page.pageNumber);

                    it('must not be a response.error', () => {
                        expect(responsePage.error).to.equal(0);
                    });
                    it('must not be a response.msg', () => {
                        expect(responsePage.msg).to.equal('');
                    });
                    it('data.pageSize must be ' + paginateSet.pageSizeExpected, () => {
                        expect(responsePage.data.pageSize).to.equal(paginateSet.pageSizeExpected);
                    });
                    it('data.pageNumber must be ' + page.pageNumberExpected + ' ' + (page.testComments || ''), () => {
                        expect(responsePage.data.pageNumber).to.equal(page.pageNumberExpected);
                    });
                    it('data.totalRows must be ' + paginateSet.totalRows, () => {
                        expect(responsePage.data.totalRows).to.equal(paginateSet.totalRows);
                    });
                    it('data.totalPages must be ' + paginateSet.totalPages, () => {
                        expect(responsePage.data.totalPages).to.equal(paginateSet.totalPages);
                    });
                    it(page.expectedItems + ' items must be returned in the page', () => {
                        expect(responsePage.data.page.length).to.equal(page.expectedItems);
                    });
                    it('content must be ok on page #' + page.pageNumberExpected, () => {
                        expect(responsePage.data.page).to.deep.equal(page.items);
                    });
                });
            });
        });
    });
});
