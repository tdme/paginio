var lodash = require('lodash');

var items = [
    'Item one',
    'Item two',
    'Item three',
    'Item fourth',
    'Item five',
    'Item six',
    'Item seven',
    'Item eight',
    'Item nine',
    'Item ten',
    'Item eleven',
    'Item twelve',
    'Item thirteen',
    'Item fourteen',
    'Item fifteen',
    'Item sixteen',
]

module.exports = {
    dataSet: items,
    invalidDataSets: {
        undefinedDataSet: {
            dataSet: undefined,
            page: [],
            totalRows: 0,
            totalPages: 0,
            pageNumber: 0,
            pageSize: 0,
            error: 1,
            msg: 'Undefined dataset',
            url: 'undefinedDataSet',
            status: 400
        },
        nullDataSet: {
            dataSet: null,
            page: [],
            totalRows: 0,
            totalPages: 0,
            pageNumber: 0,
            pageSize: 0,
            error: 1,
            msg: 'Undefined dataset',
            url: 'nullDataSet',
            status: 400
        },
        emptyDataSet: {
            dataSet: [],
            page: [],
            totalRows: 0,
            totalPages: 0,
            pageNumber: 0,
            pageSize: 0,
            error: 0,
            msg: 'Empty dataset',
            url: 'emptyDataSet',
            status: 200
        },
        objectDataSet: {
            testComments: "(Objects are non iterable dataset)",
            dataSet: {},
            page: undefined,
            totalRows: undefined,
            totalPages: undefined,
            pageNumber: undefined,
            pageSize: undefined,
            error: 0,
            msg: 'Non iterable dataset, Objects and Basic Data Types can not be iterated',
            url: 'objectDataSet',
            status: 200
        },
        intTypeDataSet: {
            testComments: "(Integers are non iterable dataset)",
            dataSet: 1,
            page: undefined,
            totalRows: undefined,
            totalPages: undefined,
            pageNumber: undefined,
            pageSize: undefined,
            error: 0,
            msg: 'Non iterable dataset, Objects and Basic Data Types can not be iterated',
            url: 'intTypeDataSet',
            status: 200
        },
        stringTypeDataSet: {
            testComments: "(Strings are non iterable dataset)",
            dataSet: '1',
            page: undefined,
            totalRows: undefined,
            totalPages: undefined,
            pageNumber: undefined,
            pageSize: undefined,
            error: 0,
            msg: 'Non iterable dataset, Objects and Basic Data Types can not be iterated',
            url: 'intTypeDataSet',
            status: 200
        }
    },
    paginateByPagenumber: {
        paginateByInvalidPageNumber: {
            testComments: "(And invalid pageNumber)",
            pageSize: 6,
            pageSizeExpected: 6,
            totalPages: 3,
            totalRows: 16,
            pages: [{
                testComments: "(It must use default value pageNumber=1 when passing undefined pageNumber)",
                pageNumber: undefined,
                pageNumberExpected: 1,
                expectedItems: 6,
                items: ['Item one', 'Item two', 'Item three', 'Item fourth', 'Item five', 'Item six']
            }, {
                testComments: "(It must use default value pageNumber=1 when passing NaN pageNumber)",
                pageNumber: 'this is a NaN',
                pageNumberExpected: 1,
                expectedItems: 6,
                items: ['Item one', 'Item two', 'Item three', 'Item fourth', 'Item five', 'Item six']
            }, {
                testComments: "(It must use default value pageNumber=1 when passing negative pageNumber)",
                pageNumber: -1,
                pageNumberExpected: 1,
                expectedItems: 6,
                items: ['Item one', 'Item two', 'Item three', 'Item fourth', 'Item five', 'Item six']
            }, {
                testComments: "(It must use default value pageNumber=1 when passing pageNumber=0)",
                pageNumber: 0,
                pageNumberExpected: 1,
                expectedItems: 6,
                items: ['Item one', 'Item two', 'Item three', 'Item fourth', 'Item five', 'Item six']
            }, {
                testComments: "(It must use default value pageNumber=1 when passing pageNumber='')",
                pageNumber: '',
                pageNumberExpected: 1,
                expectedItems: 6,
                items: ['Item one', 'Item two', 'Item three', 'Item fourth', 'Item five', 'Item six']
            }]
        },
        paginateBy6: {
            pageSize: 6,
            pageSizeExpected: 6,
            totalPages: 3,
            totalRows: 16,
            pages: [{
                pageNumber: 1,
                pageNumberExpected: 1,
                expectedItems: 6,
                items: ['Item one', 'Item two', 'Item three', 'Item fourth', 'Item five', 'Item six']
            }, {
                pageNumber: 2,
                pageNumberExpected: 2,
                expectedItems: 6,
                items: ['Item seven', 'Item eight', 'Item nine', 'Item ten', 'Item eleven', 'Item twelve']
            }, {
                pageNumber: 3,
                pageNumberExpected: 3,
                expectedItems: 4,
                items: ['Item thirteen', 'Item fourteen', 'Item fifteen', 'Item sixteen']
            }]
        },
        paginateBy4: {
            pageSize: 4,
            pageSizeExpected: 4,
            totalPages: 4,
            totalRows: 16,
            pages: [{
                pageNumber: 1,
                pageNumberExpected: 1,
                expectedItems: 4,
                items: ['Item one', 'Item two', 'Item three', 'Item fourth']
            }, {
                pageNumber: 2,
                pageNumberExpected: 2,
                expectedItems: 4,
                items: ['Item five', 'Item six', 'Item seven', 'Item eight']
            }, {
                pageNumber: 3,
                pageNumberExpected: 3,
                expectedItems: 4,
                items: ['Item nine', 'Item ten', 'Item eleven', 'Item twelve'],
            }, {
                pageNumber: 4,
                pageNumberExpected: 4,
                expectedItems: 4,
                items: ['Item thirteen', 'Item fourteen', 'Item fifteen', 'Item sixteen']
            }]
        },
        paginateBy2: {
            pageSize: 8,
            pageSizeExpected: 8,
            totalPages: 2,
            totalRows: 16,
            pages: [{
                pageNumber: 1,
                pageNumberExpected: 1,
                expectedItems: 8,
                items: ['Item one', 'Item two', 'Item three', 'Item fourth', 'Item five', 'Item six', 'Item seven', 'Item eight']
            }, {
                pageNumber: 2,
                pageNumberExpected: 2,
                expectedItems: 8,
                items: ['Item nine', 'Item ten', 'Item eleven', 'Item twelve', 'Item thirteen', 'Item fourteen', 'Item fifteen', 'Item sixteen']
            }]
        },
        paginateBy3: {
            pageSize: 3,
            pageSizeExpected: 3,
            totalPages: 6,
            totalRows: 16,
            pages: [{
                pageNumber: 1,
                pageNumberExpected: 1,
                expectedItems: 3,
                items: ['Item one', 'Item two', 'Item three']
            }, {
                pageNumber: 2,
                pageNumberExpected: 2,
                expectedItems: 3,
                items: ['Item fourth', 'Item five', 'Item six']
            }, {
                pageNumber: 3,
                pageNumberExpected: 3,
                expectedItems: 3,
                items: ['Item seven', 'Item eight', 'Item nine'],
            }, {
                pageNumber: 4,
                pageNumberExpected: 4,
                expectedItems: 3,
                items: ['Item ten', 'Item eleven', 'Item twelve']
            }, {
                pageNumber: 5,
                pageNumberExpected: 5,
                expectedItems: 3,
                items: ['Item thirteen', 'Item fourteen', 'Item fifteen']
            }, {
                pageNumber: 6,
                pageNumberExpected: 6,
                expectedItems: 1,
                items: ['Item sixteen']
            }]
        },
        paginateBy16: {
            pageSize: 16,
            pageSizeExpected: 16,
            totalPages: 1,
            totalRows: 16,
            pages: [{
                pageNumber: 1,
                pageNumberExpected: 1,
                expectedItems: 16,
                items: items
            }]
        },
        paginateBy17: {
            pageSize: 17,
            pageSizeExpected: 17,
            totalPages: 1,
            totalRows: 16,
            pages: [{
                pageNumber: 1,
                pageNumberExpected: 1,
                expectedItems: 16,
                items: items
            }]
        },
        paginateBy20: {
            pageSize: 20,
            pageSizeExpected: 20,
            totalPages: 1,
            totalRows: 16,
            pages: [{
                pageNumber: 1,
                pageNumberExpected: 1,
                expectedItems: 16,
                items: items
            }]
        },
        paginateBy10: {
            pageSize: 10,
            pageSizeExpected: 10,
            totalPages: 2,
            totalRows: 16,
            pages: [{
                pageNumber: 1,
                pageNumberExpected: 1,
                expectedItems: 10,
                items: ['Item one', 'Item two', 'Item three', 'Item fourth', 'Item five', 'Item six', 'Item seven', 'Item eight', 'Item nine', 'Item ten']
            }, {
                pageNumber: 2,
                pageNumberExpected: 2,
                expectedItems: 6,
                items: ['Item eleven', 'Item twelve', 'Item thirteen', 'Item fourteen', 'Item fifteen', 'Item sixteen']
            }]
        },
        paginateBy0: {
            testComments: "(It must use default value pageSize=10 when passing zero)",
            pageSize: 0,
            pageSizeExpected: 10,
            totalPages: 2,
            totalRows: 16,
            pages: [{
                pageNumber: 1,
                pageNumberExpected: 1,
                expectedItems: 10,
                items: ['Item one', 'Item two', 'Item three', 'Item fourth', 'Item five', 'Item six', 'Item seven', 'Item eight', 'Item nine', 'Item ten']
            }, {
                pageNumber: 2,
                pageNumberExpected: 2,
                expectedItems: 6,
                items: ['Item eleven', 'Item twelve', 'Item thirteen', 'Item fourteen', 'Item fifteen', 'Item sixteen']
            }]
        },
        paginateByMinus1: {
            testComments: "(It must use default value pageSize=10 when passing negative number)",
            pageSize: -1,
            pageSizeExpected: 10,
            totalPages: 2,
            totalRows: 16,
            pages: [{
                pageNumber: 1,
                pageNumberExpected: 1,
                expectedItems: 10,
                items: ['Item one', 'Item two', 'Item three', 'Item fourth', 'Item five', 'Item six', 'Item seven', 'Item eight', 'Item nine', 'Item ten']
            }, {
                pageNumber: 2,
                pageNumberExpected: 2,
                expectedItems: 6,
                items: ['Item eleven', 'Item twelve', 'Item thirteen', 'Item fourteen', 'Item fifteen', 'Item sixteen']
            }]
        },
        paginateByInvalidNumber: {
            testComments: "(It must use default value pageSize=10 when passing invalid number)",
            pageSize: 'this is a NaN',
            pageSizeExpected: 10,
            totalPages: 2,
            totalRows: 16,
            pages: [{
                pageNumber: 1,
                pageNumberExpected: 1,
                expectedItems: 10,
                items: ['Item one', 'Item two', 'Item three', 'Item fourth', 'Item five', 'Item six', 'Item seven', 'Item eight', 'Item nine', 'Item ten']
            }, {
                pageNumber: 2,
                pageNumberExpected: 2,
                expectedItems: 6,
                items: ['Item eleven', 'Item twelve', 'Item thirteen', 'Item fourteen', 'Item fifteen', 'Item sixteen']
            }]
        }
    },
    scrollActions: {
        paginateBy6: {
            pageSize: 6,
            pageSizeExpected: 6,
            totalPages: 3,
            totalRows: 16,
            pages: [{
                action: 'first',
                pageNumber: 1,
                pageNumberExpected: 1,
                expectedItems: 6,
                items: ['Item one', 'Item two', 'Item three', 'Item fourth', 'Item five', 'Item six']
            }, {
                action: 'next',
                pageNumber: 2,
                pageNumberExpected: 2,
                expectedItems: 6,
                items: ['Item seven', 'Item eight', 'Item nine', 'Item ten', 'Item eleven', 'Item twelve']
            }, {
                action: 'next',
                pageNumber: 3,
                pageNumberExpected: 3,
                expectedItems: 4,
                items: ['Item thirteen', 'Item fourteen', 'Item fifteen', 'Item sixteen']
            }, {
                action: 'last',
                pageNumber: 3,
                pageNumberExpected: 3,
                expectedItems: 4,
                items: ['Item thirteen', 'Item fourteen', 'Item fifteen', 'Item sixteen']
            }, {
                action: 'first',
                pageNumber: 1,
                pageNumberExpected: 1,
                expectedItems: 6,
                items: ['Item one', 'Item two', 'Item three', 'Item fourth', 'Item five', 'Item six']
            }]
        },
        paginateBy4FromLastToFirst: {
            testComments: "(Starting on Last doing back upto First)",
            pageSize: 4,
            pageSizeExpected: 4,
            totalPages: 4,
            totalRows: 16,
            pages: [{
                action: 'last',
                pageNumber: 4,
                pageNumberExpected: 4,
                expectedItems: 4,
                items: ['Item thirteen', 'Item fourteen', 'Item fifteen', 'Item sixteen']
            }, {
                action: 'back',
                pageNumber: 3,
                pageNumberExpected: 3,
                expectedItems: 4,
                items: ['Item nine', 'Item ten', 'Item eleven', 'Item twelve'],
            }, {
                action: 'back',
                pageNumber: 2,
                pageNumberExpected: 2,
                expectedItems: 4,
                items: ['Item five', 'Item six', 'Item seven', 'Item eight']
            }, {
                testComments: "(This must be the First page after doing Back)",
                action: 'back',
                pageNumber: 1,
                pageNumberExpected: 1,
                expectedItems: 4,
                items: ['Item one', 'Item two', 'Item three', 'Item fourth']
            }]
        },
        paginateBy3FromFirstToLastDoingNext: {
            pageSize: 3,
            pageSizeExpected: 3,
            totalPages: 6,
            totalRows: 16,
            pages: [{
                action: 'first',
                pageNumber: 1,
                pageNumberExpected: 1,
                expectedItems: 3,
                items: ['Item one', 'Item two', 'Item three']
            }, {
                action: 'next',
                pageNumber: 2,
                pageNumberExpected: 2,
                expectedItems: 3,
                items: ['Item fourth', 'Item five', 'Item six']
            }, {
                action: 'next',
                pageNumber: 3,
                pageNumberExpected: 3,
                expectedItems: 3,
                items: ['Item seven', 'Item eight', 'Item nine'],
            }, {
                action: 'next',
                pageNumber: 4,
                pageNumberExpected: 4,
                expectedItems: 3,
                items: ['Item ten', 'Item eleven', 'Item twelve']
            }, {
                action: 'next',
                pageNumber: 5,
                pageNumberExpected: 5,
                expectedItems: 3,
                items: ['Item thirteen', 'Item fourteen', 'Item fifteen']
            }, {
                testComments: "(This must be the Last page after doing Next)",
                action: 'next',
                pageNumber: 6,
                pageNumberExpected: 6,
                expectedItems: 1,
                items: ['Item sixteen']
            }]
        },
        paginateBy3FailingFromLastToNext: {
            testComments: "(Forcing fail from Last page after doing Next)",
            pageSize: 3,
            pageSizeExpected: 3,
            totalPages: 6,
            totalRows: 16,
            pages: [{
                action: 'last',
                pageNumber: 6,
                pageNumberExpected: 6,
                expectedItems: 1,
                items: ['Item sixteen']
            }, {
                testComments: "(First fail and force to return pageNumber=6, invalid move forward)",
                action: 'next',
                pageNumber: 6,
                pageNumberExpected: 6,
                expectedItems: 1,
                items: ['Item sixteen'],
                error: 1,
                msg: 'The cursor is on last page, you can not move forward'
            }, {
                testComments: "(Second fail and force to return pageNumber=6, invalid move forward)",
                action: 'next',
                pageNumber: 6,
                pageNumberExpected: 6,
                expectedItems: 1,
                items: ['Item sixteen'],
                error: 1,
                msg: 'The cursor is on last page, you can not move forward'
            }, {
                testComments: "(This must be the page #5 after doing Back from Last)",
                action: 'back',
                pageNumber: 5,
                pageNumberExpected: 5,
                expectedItems: 3,
                items: ['Item thirteen', 'Item fourteen', 'Item fifteen']
            }]
        },
        paginateBy3FailingFromFirstToBack: {
            testComments: "(Forcing fail from First page after doing Back)",
            pageSize: 3,
            pageSizeExpected: 3,
            totalPages: 6,
            totalRows: 16,
            pages: [{
                action: 'first',
                pageNumber: 1,
                pageNumberExpected: 1,
                expectedItems: 3,
                items: ['Item one', 'Item two', 'Item three']
            }, {
                action: 'next',
                pageNumber: 2,
                pageNumberExpected: 2,
                expectedItems: 3,
                items: ['Item fourth', 'Item five', 'Item six']
            }, {
                testComments: "(Must be the First page after doing Back)",
                action: 'back',
                pageNumber: 1,
                pageNumberExpected: 1,
                expectedItems: 3,
                items: ['Item one', 'Item two', 'Item three']
            }, {
                testComments: "(First fail and force to return pageNumber=1, invalid move backward)",
                action: 'back',
                pageNumber: 1,
                pageNumberExpected: 1,
                expectedItems: 3,
                items: ['Item one', 'Item two', 'Item three'],
                error: 1,
                msg: 'The cursor is on first page, you can not move backward'
            }, {
                testComments: "(Second fail and force to return pageNumber=1, invalid move backward)",
                action: 'back',
                pageNumber: 1,
                pageNumberExpected: 1,
                expectedItems: 3,
                items: ['Item one', 'Item two', 'Item three'],
                error: 1,
                msg: 'The cursor is on first page, you can not move backward'
            }]
        },

    }
};
