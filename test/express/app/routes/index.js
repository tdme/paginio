var express = require('express');
var router = express.Router();
var Scroller = require('../../../../index').Scroller,
    testData = require('../../../data/testData');

/* GET home page. */
router.get('/', function(req, res, next) {
    Scroller.scroll(
        req,
        res,
        'your_buffer_session_key',
        function(cb) {
            console.log('          Running search function');

            switch (req.headers['datasettesttype']) {
                case 'undefinedDataSet':
                    cb();
                    break;
                case 'nullDataSet':
                    cb(null);
                    break;
                case 'emptyDataSet':
                    cb([]);
                    break;
                case 'objectDataSet':
                    cb({name: 'dummy object'});
                    break;
                case 'intTypeDataSet':
                    cb(1);
                    break;
                case 'stringTypeDataSet':
                    cb('1');
                    break;
                default:
                    cb(testData.dataSet);
            }
        },
        function(response) {
            // Callback
            res.json(response);
        }
    );

});

module.exports = router;
