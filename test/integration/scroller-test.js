/**
 * Created by cecheveria on 10/21/16.
 */
var request = require('supertest');
var testData = require('../data/testData'),
    webApp = require('../express/app/app')
    ;
var invalidDataSets = Object.keys(testData.invalidDataSets).map(function(key) {
        return testData.invalidDataSets[key];
    });
var paginateByPagenumberTestCases = Object.keys(testData.paginateByPagenumber).map(function(key) {
        return testData.paginateByPagenumber[key];
    });
var scrollActionsTestCases = Object.keys(testData.scrollActions).map(function(key) {
        return testData.scrollActions[key];
    });
var items = testData.dataSet;

describe('Scroll Object on Express App', ()=> {
    describe('Scrolling invalid datasets', () => {

        invalidDataSets
        .forEach(function (paginateSet) {
            describe('Buffer with "' +paginateSet.dataSet+ '" dataSet ' + (paginateSet.testComments || ''), () => {
                var response;

                it('must respond with a ' +paginateSet.status+ ' status code', (done)=> {
                    var url = '/?refresh=Y'

                    request(webApp).get(url).set({'datasettesttype': paginateSet.url}).expect(function (res) {
                        response = res;
                        expect(response.status).to.equal(paginateSet.status);
                    }).end(done);
                });

                it('must be response.error=' + paginateSet.error, (done) => {
                    expect(response.body.error).to.equal(paginateSet.error);
                    done();
                });
                it('must be response.msg="' + paginateSet.msg + '"', (done) => {
                    expect(response.body.msg).to.equal(paginateSet.msg);
                    done();
                });
                it('data.pageSize must be ' + paginateSet.pageSize, (done) => {
                    expect(response.body.data.pageSize).to.equal(paginateSet.pageSize);
                    done();
                });
                it('data.pageNumber must be ' + paginateSet.pageNumber, (done) => {
                    expect(response.body.data.pageNumber).to.equal(paginateSet.pageNumber);
                    done();
                });
                it('data.totalRows must be ' + paginateSet.totalRows, (done) => {
                    expect(response.body.data.totalRows).to.equal(paginateSet.totalRows);
                    done();
                });
                it('data.totalPages must be ' + paginateSet.totalPages, (done) => {
                    expect(response.body.data.totalPages).to.equal(paginateSet.totalPages);
                    done();
                });
                it((paginateSet.page && paginateSet.page.length) + ' items must be returned in the page', (done) => {
                    expect( (response.body.data.page && response.body.data.page.length) ).to.equal(paginateSet.page && paginateSet.page.length);
                    done();
                });
                it('content must be empty on current page', (done) => {
                    expect(response.body.data.page).to.deep.equal(paginateSet.page);
                    done();
                });
            });
        });
    });

    describe('Scrolling using pageNumber', () => {

        paginateByPagenumberTestCases
        .forEach(function (paginateSet) {

            describe('Scrolling with "' + paginateSet.pageSize + '" items per page ' + (paginateSet.testComments || ''), () => {
                var cookie;

                it('must respond with a 200 status code, on first time query', (done)=> {
                    request(webApp).get('/?pageSize=' + paginateSet.pageSize).expect(function (response) {
                        cookie = response.headers['set-cookie']; //Setting the cookie to force persist the session
                        expect(response.status).to.equal(200);
                    })
                    .end(done);
                });

                paginateSet.pages.forEach(function (page) {
                    describe('Getting page #' + page.pageNumber, () => {
                        var url = '/?pageNumber='+page.pageNumber

                        it('data.page must be an instance of Array', (done)=> {
                            request(webApp).get(url).set('Cookie', cookie).expect(function (response) {
                                expect(response.body.data.page).to.be.instanceOf(Array);
                            }).end(done);
                        });
                        it('must not be a response.error', (done)=> {
                            request(webApp).get(url).set('Cookie', cookie).expect(function (response) {
                                expect(response.body.error).to.equal(0);
                            }).end(done);
                        });
                        it('must not be a response.msg', (done)=> {
                            request(webApp).get(url).set('Cookie', cookie).expect(function (response) {
                                expect(response.body.msg).to.equal('');
                            }).end(done);
                        });
                        it('data.pageSize must be ' + paginateSet.pageSizeExpected, (done)=> {
                            request(webApp).get(url).set('Cookie', cookie).expect(function (response) {
                                expect(response.body.data.pageSize).to.equal(paginateSet.pageSizeExpected);
                            }).end(done);
                        });
                        it('data.pageNumber must be ' + page.pageNumberExpected + ' ' + (page.testComments || ''), (done)=> {
                            request(webApp).get(url).set('Cookie', cookie).expect(function (response) {
                                expect(response.body.data.pageNumber).to.equal(page.pageNumberExpected);
                            }).end(done);
                        });
                        it('data.totalRows must be ' + paginateSet.totalRows, (done)=> {
                            request(webApp).get(url).set('Cookie', cookie).expect(function (response) {
                                expect(response.body.data.totalRows).to.equal(paginateSet.totalRows);
                            }).end(done);
                        });
                        it('data.totalPages must be ' + paginateSet.totalPages, (done)=> {
                            request(webApp).get(url).set('Cookie', cookie).expect(function (response) {
                                expect(response.body.data.totalPages).to.equal(paginateSet.totalPages);
                            }).end(done);
                        });
                        it(page.expectedItems + ' items must be returned in the page', (done)=> {
                            request(webApp).get(url).set('Cookie', cookie).expect(function (response) {
                                expect(response.body.data.page.length).to.equal(page.expectedItems);
                            }).end(done);
                        });
                        it('content must be ok on page #' + page.pageNumber, (done)=> {
                            request(webApp).get(url).set('Cookie', cookie).expect(function (response) {
                                expect(response.body.data.page).to.deep.equal(page.items);
                            }).end(done);
                        });

                        it('must return page #'+page.pageNumber +' as current page when pageNumber was not passed', (done)=> {
                            request(webApp).get('/').set('Cookie', cookie).expect(function (response) {
                                expect(response.body.data.pageNumber).to.equal(page.pageNumberExpected);
                            }).end(done);
                        });
                    });
                });

                paginateSet.pages.forEach(function (page) {
                    describe('Refreshing query and getting page #' + page.pageNumber, () => {
                        var url = '/?refresh=Y&pageNumber='+page.pageNumber

                        it('data.page must be an instance of Array', (done)=> {
                            request(webApp).get(url).set('Cookie', cookie).expect(function (response) {
                                expect(response.body.data.page).to.be.instanceOf(Array);
                            }).end(done);
                        });
                        it('must return page #' +page.pageNumberExpected+ ' when refresh=Y and pageNumber='+page.pageNumber, (done)=> {
                            request(webApp).get(url).set('Cookie', cookie).expect(function (response) {
                                expect(response.body.data.pageNumber).to.equal(page.pageNumberExpected);
                            }).end(done);
                        });
                        it('must return page #' +1+ ' when refresh=Y and no pageNumber', (done)=> {
                            request(webApp).get('/?refresh=Y').set('Cookie', cookie).expect(function (response) {
                                expect(response.body.data.pageNumber).to.equal(1);
                            }).end(done);
                        });
                    });
                });
            });
        });
    });

    describe('Scrolling using actions', () => {

        scrollActionsTestCases
        .forEach(function (paginateSet) {

            describe('Scrolling actions with "' + paginateSet.pageSize + '" items per page ' + (paginateSet.testComments || ''), () => {
                var cookie,
                    response;

                it('must respond with a 200 status code, on first time query', (done)=> {
                    var url = '/?pageSize='+paginateSet.pageSize

                    request(webApp).get(url).expect(function (res) {
                        response = res;
                        cookie = response.headers['set-cookie']; //Setting the cookie to force persist the session
                        expect(response.status).to.equal(200);
                    })
                    .end(done);
                });

                paginateSet.pages.forEach(function (page) {
                    describe('Getting page #' + page.pageNumber + ' scroll=' + page.action, () => {
                        var url = '/?scroll='+page.action

                        it('data.page must be an instance of Array', (done)=> {
                            request(webApp).get(url).set('Cookie', cookie).expect(function (res) {
                                response = res;
                                expect(response.body.data.page).to.be.instanceOf(Array);
                            }).end(done);
                        });
                        it('must be response.error=' + (page.error || 0), (done)=> {
                            expect(response.body.error).to.equal(page.error || 0);
                            done();
                        });
                        it('must be response.msg="' +(page.msg || '')+ '"', (done)=> {
                            expect(response.body.msg).to.equal(page.msg || '');
                            done();
                        });
                        it('data.pageSize must be ' + paginateSet.pageSizeExpected, (done)=> {
                            expect(response.body.data.pageSize).to.equal(paginateSet.pageSizeExpected);
                            done();
                        });
                        it('data.pageNumber must be ' + page.pageNumberExpected + ' ' + (page.testComments || ''), (done)=> {
                            expect(response.body.data.pageNumber).to.equal(page.pageNumberExpected);
                            done();
                        });
                        it('data.totalRows must be ' + paginateSet.totalRows, (done)=> {
                            expect(response.body.data.totalRows).to.equal(paginateSet.totalRows);
                            done();
                        });
                        it('data.totalPages must be ' + paginateSet.totalPages, (done)=> {
                            expect(response.body.data.totalPages).to.equal(paginateSet.totalPages);
                            done();
                        });
                        it(page.expectedItems + ' items must be returned in the page', (done)=> {
                            expect(response.body.data.page.length).to.equal(page.expectedItems);
                            done();
                        });
                        it('content must be ok on page #' + page.pageNumber, (done)=> {
                            expect(response.body.data.page).to.deep.equal(page.items);
                            done();
                        });

                        it('must return page #'+page.pageNumber +' as current page when pageNumber was not passed', (done)=> {
                            request(webApp).get('/').set('Cookie', cookie).expect(function (response) {
                                expect(response.body.data.pageNumber).to.equal(page.pageNumberExpected);
                            }).end(done);
                        });
                    });
                });
            });
        });
    });
});
