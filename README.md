[![Npm version](https://badge.fury.io/js/paginio.svg)](https://badge.fury.io/js/paginio)

# [Paginio](https://www.npmjs.com/package/paginio)

> A **Pagination Tool** for **arrays on Node.js**, and other things like easy datasets pagination on **Express Routes**,
 see more at [Paginio Npm](https://www.npmjs.com/package/paginio).


## How to install
```bash
npm install --save paginio
```

## Buffer Library
The Buffer allows you to paginate arrays based on a given `pageSize` parameter. The parameter must be an `int value` greater than zero, else case it will be defaulted as `pageSize=10`.

#### Response Object
Every time we get a page from the buffer, a `Response` object will be returned, it will contain the details about how the buffer is being paginated:
```javascript
{
    data: {
        page: <Array>,          // The corresponding items for the requested page
        totalRows: <int>,       // How many items are in the current page
        totalPages: <int>,      // How many pages will be returned
        pageNumber: <int>,      // Actual requested page number
        pageSize: <int>         // The page size used to paginate the buffer
    },
    error: <int>,               // States if an error has occurred when paginating the buffer (0 is ok, any other numbers means an error)
    msg: <string>               // Message error or empty if there are not errors
}
```


#### Buffer samples

```javascript
var Buffer = require('paginio').Buffer.create();
var pageSize = 5;
var items = [
    'Item one',
    'Item two',
    'Item three',
    'Item fourth',
    'Item five',
    'Item six',
    'Item seven',
    'Item eight',
    'Item nine',
    'Item ten',
    'Item eleven',
    'Item twelve',
    'Item thirteen',
    'Item fourteen',
    'Item fifteen',
    'Item sixteen',
    ];
```

Given the previous code snippet, the corresponding response for each page will be as shown down below:
```javascript
// Getting first page
var responsePage = Buffer.getPage(items, pageSize, 1);
// Response will be
{
    data: {
        page: ['Item one', 'Item two', 'Item three', 'Item fourth', 'Item five'],
        totalRows: 16,
        totalPages: 4,
        pageNumber: 1,
        pageSize: 5
    },
    error: 0,
    msg: ""
}

// Getting second page
responsePage = Buffer.getPage(items, pageSize, 2);
// Response will be
{
    data: {
        page: ['Item six', 'Item seven', 'Item eight', 'Item nine', 'Item ten'],
        totalRows: 16,
        totalPages: 4,
        pageNumber: 2,
        pageSize: 5
    },
    error: 0,
    msg: ""
}

// Getting second page
responsePage = Buffer.getPage(items, pageSize, 3);
// Response will be
{
    data: {
        page: ['Item eleven', 'Item twelve', 'Item thirteen', 'Item fourteen', 'Item fifteen'],
        totalRows: 16,
        totalPages: 4,
        pageNumber: 3,
        pageSize: 5
    },
    error: 0,
    msg: ""
}

// Getting second page
responsePage = Buffer.getPage(items, pageSize, 4);
// Response will be
{
    data: {
        page: ['Item sixteen'],
        totalRows: 16,
        totalPages: 4,
        pageNumber: 4,
        pageSize: 5
    },
    error: 0,
    msg: ""
}
```

## Scroller using actions
It allows you to paginate session buffers based on a given `scroll` request parameter, that must be one `scroll action` like `first`, `next`, `back` or `last`. Those actions will let you easily navigate your buffer from current session, actually supported only **Express Sessions**.

Every time a scroll request is invoked it will return a [Response Object](#response-object).

#### Action `refresh`
It will let you execute a query and save the new dataset on the **Session Buffer**, after running the query it will set the cursor on first page if the `pageNumber` parameter was not passed on the request, otherwise, it will set the cursor on the requested `pageNumber`. You must send a request like `http://localhost:3000/?refresh=Y&pageSize=5`, then it will return the response shown bellow:
```javascript
{
    data: {
        page: ['Item one', 'Item two', 'Item three', 'Item fourth', 'Item five'],
        totalRows: 16,
        totalPages: 4,
        pageNumber: 1,
        pageSize: 5
    },
    error: 0,
    msg: ""
}
```

#### Action `first`
It will set the cursor on first page, for that you need to send a request like `http://localhost:3000/?scroll=first`, you will get the response shown bellow:
```javascript
{
    data: {
        page: ['Item one', 'Item two', 'Item three', 'Item fourth', 'Item five'],
        totalRows: 16,
        totalPages: 4,
        pageNumber: 1,
        pageSize: 5
    },
    error: 0,
    msg: ""
}
```

#### Action `next`
It will set the cursor on next page, for that you need to send a request like `http://localhost:3000/?scroll=next`, you will get the response shown bellow:
```javascript
{
    data: {
        page: ['Item six', 'Item seven', 'Item eight', 'Item nine', 'Item ten'],
        totalRows: 16,
        totalPages: 4,
        pageNumber: 2,
        pageSize: 5
    },
    error: 0,
    msg: ""
}
```

#### Action `back`
It will set the cursor on previous page, for that you need to send a request like `http://localhost:3000/?scroll=back`, you will get the response shown bellow:
```javascript
{
    data: {
        page: ['Item one', 'Item two', 'Item three', 'Item fourth', 'Item five'],
        totalRows: 16,
        totalPages: 4,
        pageNumber: 1,
        pageSize: 5
    },
    error: 0,
    msg: ""
}
```

#### Action `last`
It will set the cursor on last page, for that you need to send a request like `http://localhost:3000/?scroll=last`, you will get the response shown bellow:
```javascript
{
    data: {
        page: ['Item sixteen'],
        totalRows: 16,
        totalPages: 4,
        pageNumber: 4,
        pageSize: 5
    },
    error: 0,
    msg: ""
}
```
