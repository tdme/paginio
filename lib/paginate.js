/**
 * Created by cecheveria on 06/21/16.
 */
function Page(page) {
    this.page = [];
    this.totalRows = 0;
    this.totalPages = 0;
    this.pageNumber = 0;
    this.pageSize = 0;

    if (page) {
        this.page = page.page ? page.page : [];
        this.totalRows = page.totalRows ? page.totalRows : 0;
        this.totalPages = page.totalPages ? page.totalPages : 0;
        this.pageNumber = page.pageNumber ? page.pageNumber : 0;
        this.pageSize = page.pageSize ? page.pageSize : 0;
    }
}
Page.create = function (page) { return new Page(page); }

function ResponseBase(response) {
    this.data = "";
    this.error = 0;
    this.msg = "";

    if (response) {
        this.error = response.error ? response.error : 0;
        this.msg = response.msg ? response.msg : "";
        this.data = response.data ? response.data : "";
    }
}
ResponseBase.create = function (response) { return new ResponseBase(response); }

function ResponsePage(page) {
    this.data = Page.create();
    this.error = 0;
    this.msg = "";

    if (page) {
        /*if(Object.prototype.toString.call( page ) == "[object Array]") {
        }
        else {*/
            this.data.page = page.page ? page.page : [];
            this.data.totalRows = page.totalRows ? page.totalRows : 0;
            this.data.totalPages = page.totalPages ? page.totalPages : 0;
            this.data.pageNumber = page.pageNumber ? page.pageNumber : 0;
            this.data.pageSize = page.pageSize ? page.pageSize : 0;
        //}
    }
}
ResponsePage.create = function (page) { return new ResponsePage(page); }

function Buffer() {
}
Buffer.create = function () { return new Buffer(); }
Buffer.prototype.getPage = function (data, pageSize, pageNumber, fromIndex) {

    var responsePage = ResponsePage.create();

    pageSize = (!pageSize || isNaN(pageSize) || pageSize<=0) ? 10 : pageSize
    pageNumber = (!pageNumber || isNaN(pageNumber) || pageNumber<=0) ? 1 : pageNumber

    if(!data) {
        responsePage.error = 1
        responsePage.msg = "Undefined dataset"
    }
    else if(Object.prototype.toString.call( data ) != "[object Array]") {
        responsePage.data = data
        responsePage.error = 0
        responsePage.msg = "Non iterable dataset, Objects and Basic Data Types can not be iterated"
    }
    else if(data.length == 0) {
        responsePage.error = 0
        responsePage.msg = "Empty dataset"
    }
    else {
        if(pageNumber <= 0) {
            responsePage.error = 1;
            responsePage.msg = "Page does not exists, it must be great than cero";
        }
        else {
            var totalPages = 0;
            totalPages = pageSize == 1 ? data.length : (data.length/pageSize) + .499;
            totalPages = Math.round(totalPages);

            if(pageNumber > totalPages) {
                responsePage.data.page = [];
                responsePage.error = 1;
                responsePage.msg = "Page does not exists, it must be less or equal than " + totalPages;
            }
            else {
                fromIndex = (pageNumber-1) * pageSize;
                var toIndex = (parseInt(fromIndex) + parseInt(pageSize)) < data.length ? parseInt(fromIndex)+parseInt(pageSize) : data.length;

                responsePage.data.page = data.slice(fromIndex, toIndex);
            }

            responsePage.data.totalRows = data.length;
            responsePage.data.totalPages = Math.round(totalPages);
            responsePage.data.pageNumber = parseInt(pageNumber);
            responsePage.data.pageSize = parseInt(pageSize);
        }
    }

    return responsePage;
}

module.exports.Page = Page;
module.exports.ResponseBase = ResponseBase;
module.exports.ResponsePage = ResponsePage;
module.exports.Buffer = Buffer;
