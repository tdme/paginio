/**
 * Created by cecheveria on 06/21/16.
 */
var Paginate = require('./paginate');

/**
 * List of commands used to work with rest methods
 *
 * @type {{Rest: {list: string, current: string, first: string, last: string, next: string, back: string}}}
 */
const COMMANDS = {
    Rest: {
        // list: "list",
        // current: "current",
        first: "first",
        last: "last",
        next: "next",
        back: "back"
    }
}

function scroll(req, res, sessionKey, searchFn, cb) {

    var pageSize = req.query.pageSize,
        pageNumber = req.query.pageNumber,
        scrollCommand = req.query.scroll,
        refresh = req.query.refresh || 'n',
        sess = req.session,
        response
        buffer = sess[sessionKey]
        ;

    if(refresh.toLowerCase()=='y' || !buffer) {
        // console.log('scroll refreshing, refresh:', refresh, 'buffer is:', buffer);

        searchFn(function(dataSet) {

            if(Object.prototype.toString.call( dataSet ) == "[object Array]") {

                if (refresh.toLowerCase()=='y')
                    // Default to first page if no req.query.pageNumber was passed
                    pageNumber = pageNumber ? pageNumber : 1;
                else
                    // Use session value if no req.query.pageNumber was passed, else case use default pageNumber from library
                    pageNumber = pageNumber ? pageNumber : (sess[sessionKey] && sess[sessionKey].data ? sess[sessionKey].data.pageNumber : undefined);

                pageSize = pageSize ? pageSize : (sess[sessionKey] && sess[sessionKey].data ? sess[sessionKey].data.pageSize : undefined);

                response = Paginate.Buffer.create().getPage(dataSet, pageSize, pageNumber);//1/*<== first page*/);

                // Save the entire new dataset on data.page session field
                sess[sessionKey] = response
                sess[sessionKey].data.page = dataSet;

                doGetPage(scrollCommand, pageSize, pageNumber, sess, sessionKey, res, cb);
            }
            else {
                response = Paginate.Buffer.create().getPage(dataSet);

                if(response.error == 0)
                    cb(response);
                else
                    res.status(400).json(response);
            }
        });
    }
    else {
        // console.log('scroll from session');
        doGetPage(scrollCommand, pageSize, pageNumber, sess, sessionKey, res, cb);
    }
}

function doGetPage(scrollCommand, pageSize, pageNumber, sess, sessionKey, res, cb) {

    var buffer = sess[sessionKey],
        response;

    if(isRestCommand(scrollCommand)) {
        response = pageUsingScroller(scrollCommand, sess, sessionKey);
    }
    else {
        response = pageUsingPageNumber(buffer.data.page, pageSize, pageNumber, sess, sessionKey);
    }

    if(response.error == 0)
        cb(response);
    else
        res.status(400).json(response);
}

/**
 * Validate if it's a rest command
 *
 * @param scrollCommand
 * @return {boolean}
 */
function isRestCommand(scrollCommand) {
    return COMMANDS.Rest[scrollCommand] ? true : false;
}

function pageUsingPageNumber(data, pageSize, pageNumber, sess, sessionKey) {

    pageSize = pageSize ? pageSize : sess[sessionKey].data.pageSize;
    pageNumber = pageNumber ? pageNumber : sess[sessionKey].data.pageNumber;

    var response = Paginate.Buffer.create().getPage(data, pageSize, pageNumber);

    // Save the new cursor page number
    sess[sessionKey].data.pageNumber = response.data.pageNumber;
    sess[sessionKey].data.pageSize = response.data.pageSize;
    // sess[sessionKey] = response
    // sess[sessionKey].data.page = data

    return response;
}

function pageUsingScroller(scrollCommand, sess, sessionKey) {
    var buffer = sess[sessionKey],
        response,
        pageNum,
        error,
        message;

    // console.log('pageUsingScroller:', scrollCommand/*, buffer, sess, sessionKey*/);
    if(buffer) {
        if(scrollCommand == "current") {
            pageNum = buffer.data.pageNumber;
        }
        else if(scrollCommand == "first") {
            pageNum = 1;
        }
        else if(scrollCommand == "last") {
            pageNum = buffer.data.totalPages;
        }
        else if(scrollCommand == "next") {
            if (buffer.data.pageNumber == buffer.data.totalPages) {
                error = 1
                message = "The cursor is on last page, you can not move forward"
                pageNum = buffer.data.pageNumber
            }
            else{
                pageNum = buffer.data.pageNumber + 1;
            }
        }
        else if(scrollCommand == "back") {
            if (buffer.data.pageNumber == 1) {
                error = 1
                message = "The cursor is on first page, you can not move backward"
                pageNum = buffer.data.pageNumber
            }
            else{
                pageNum = buffer.data.pageNumber - 1;
            }
        }

        //Get the page
        response = Paginate.Buffer.create().getPage(buffer.data.page, buffer.data.pageSize, pageNum);

        // Evaluate if pagination was successful
        if (error) {
            response.error = 1
            response.msg = message
        }
        // Save the new page number
        sess[sessionKey].data.pageNumber = pageNum;
    }
    else {
        // Controlar si el buffer no se encuentra en la sesion, enviar mensaje de error.
        response = Paginate.Buffer.create().getPage(undefined, pageSize, 1);
        response.msg = "Buffer not found or have been expired";
    }

    return response;
}

module.exports.isRestCommand = isRestCommand;
module.exports.scroll = scroll;
